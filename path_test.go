package ast

import (
   "os"
   "path/filepath"
   "testing"
   "github.com/spf13/viper"

)

func Test_paths(t *testing.T){
   name := "Test_paths"
   data := []struct{
      path string
      name string
      fname string
      ext string
      dir string
   }{
      {
         "source/index.rst",
         "index", "index.rst", ".rst", "source",
      }, {
         "example.xml",
         "example", "example.xml", ".xml", ".",
      }, {
         "source/includes/text.txt",
         "text", "text.txt", ".txt", "source/includes",
      },
   }
   wd, err := os.Getwd()
   if err != nil {
      t.Fatal("Failed to set working directory: ", err)
   }
   viper.Set("cwd", wd)
   viper.Set("rst.ignore-stat-failure", true)

   for pos, datum := range data {
      abs := filepath.Join(wd, datum.path)
      p := NewPath(abs)

      if p.Abs != abs {
         t.Fatalf(
            "%s %d: failed to set absolute path: %q != %q",
            name, pos, p.Abs, abs)
      }
      if p.Rel != datum.path {
         t.Fatalf(
            "%s %d: failed to set relative path: %q != %q",
            name, pos, p.Rel, datum.path)
      }
      if p.File != datum.fname {
         t.Fatalf(
            "%s %d: failed to set filename: %q != %q",
            name, pos, p.File, datum.fname)
      }
      if p.Name != datum.name {
         t.Fatalf(
            "%s %d: failed to set name: %q != %q",
            name, pos, p.Name, datum.name)
      }

      if p.Ext != datum.ext {
         t.Fatalf(
            "%s %d: failed to set extension: %q != %q",
            name, pos, p.Ext, datum.ext)
      }
      if p.Dir != datum.dir {
         t.Fatalf(
            "%s %d: failed to set parent directory: %q != %q",
            name, pos, p.Dir, datum.dir)
      }
   }
}
