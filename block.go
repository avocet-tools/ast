package ast

import (
   sitter "github.com/smacker/go-tree-sitter"
)

type Block struct {
   Idrefs []string `json:"identity_references" yaml:"identity_references" bson:"identity_references"`
   Type BType `json:"block_type" yaml:"type" bson:"block_type"`
   Con *Content `json:"content" yaml:"content" bson:"content"`

   Pos *Position `json:"position" yaml:"position" bson:"position"`
}

func NewBlock(t BType) *Block {
   return &Block{
      Type: t,
      Idrefs: []string{},
      Con: NewContent(),
   }
}

func (b *Block) Position(node *sitter.Node){
   b.Pos = getPosition(node)
}

type BType int

const (
   BlockUnset BType = iota
   Para
   UList
   OList
   DList
   ListItem
   Literal
   LineBlock
   Line
   Quote
   Footnote
   Citation
   Target
   Directive
)  
