package ast

type Content struct {
   Inline []*Element `json:"content_inline" yaml:"inline" bson:"inline"`
   Meta map[string]any `json:"content_metadata" yaml:"metadata" bson:"metadata"`
   Int map[string]any `json:"content_internal" yaml:"content_internal" bson:"content_internal"`
   Data []*Block `json:"content_data" yaml:"data" bson:"data"`
   Sects []*Section `json:"content_sections" yaml:"sections" bson:"sections"`
}

func NewContent() *Content {
   return &Content{
      Inline: []*Element{},
      Meta: make(map[string]any),
      Int: make(map[string]any),
      Data: []*Block{},
      Sects: []*Section{},
   }
}

func (c *Content) Add(blk *Block){
   blk.Con.InlineText()
   c.Data = append(c.Data, blk)
}

func (c *Content) Insert(e *Element){
   c.Inline = append(c.Inline, e)
}


func (c *Content) InlineText(){

   size := len(c.Inline)
   sinline := []*Element{}
   for i := 0; i < size; i++ {
      e := c.Inline[i]
      if e.IntType == Text && i+1 < size {
         for i+1 < size {
            f := c.Inline[i+1]
            if f.IntType != Text {
               break
            }
            e.Append(f)
            i++
         }
      }
      sinline = append(sinline, e)
   }
   c.Inline = sinline
}

func (e *Element) Append(f *Element){
   e.Text = e.Text + f.Text
   e.Pos.End = f.Pos.End
}


func (c *Content) Sect(sec *Section){
   c.Sects = append(c.Sects, sec)
}
