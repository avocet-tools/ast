package ast
import "time"
type Document struct {
   Path *Path `json:"path" yaml:"path" bson:"path"`
   Con *Content `json:"content" yaml:"content" bson:"content"`
   Valid bool `json:"valid" yaml:"valid" bson:"-"`
   ParseTime time.Duration `json:"parse_time" yaml:"parse_time" bson:"parse_time"`
}

func NewDocument(p *Path) *Document {
   doc := &Document{
      Path: p,
      Con: NewContent(),
   }
   return doc
}
