package ast
import (
   "os"
   "path"
   "path/filepath"
   "strings"
   "time"
   "github.com/spf13/viper"
   "github.com/charmbracelet/log"
)

type Path struct {
   Name string `json:"name" yaml:"name" bson:"name"`
   Ext string `json:"ext" yaml:"ext" bson:"ext"`
   Dir string `json:"directory" yaml:"directory" bson:"directory"`
   File string `json:"file" yaml:"file" bson:"file"`
   Rel string `json:"path_relative" yaml:"path_relative" bson:"path_relative"`
   Abs string `json:"path_absolute" yaml"path_absolute" bson:"path_absolute"`
   Mtime time.Time `json:"mtime" yaml"mtime" bson:"mtime"`
}

func NewPath(f string) (*Path, bool) {

   fi, err := os.Stat(f)
   ig := viper.GetBool("rst.ignore-stat-failure")
   if err != nil && !ig{ 
      log.Error(err)
      return &Path{}, false 
   }

   return NewPathWithStat(f, fi) 
}

func NewPathWithStat(f string, fi os.FileInfo) (*Path, bool) {
   rel, err := filepath.Rel(viper.GetString("cwd"), f)
   if err != nil {
      log.Error("Error occurred while rendering relative path: ", err)
      rel = f
   }
   fname := path.Base(f)
   ext := filepath.Ext(fname)
   name := strings.TrimSuffix(fname, ext)
   p := &Path{
      Abs: f,
      Rel: rel,
      File: fname,
      Ext: ext,
      Name: name,
      Dir: filepath.Dir(rel),
      Mtime: fi.ModTime(),
   }
   return p, true
}

func (p *Path) IsSrc() bool {
   if p.Ext == ".rst" || p.Ext == ".txt" {
      return true
   }
   exts := []string{}
   viper.UnmarshalKey("rst.exts", &exts)
   for _, ext := range exts {
      if ext == p.Ext {
         return true
      }
   }
   return false
}
