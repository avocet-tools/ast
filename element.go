package ast

import (
   "fmt"
)

type ElementType int
type Element struct {
   IntType ElementType `json:"element_type" yaml:"element_type bson:"element_type"`
   Text string `json:"text" yaml:"text" bson:"text"`

   // Positional Information
   Pos *Position `json:"position" yaml:"position" bson:"position"`

   Key string `json:"key" yaml:"key" bson:"key"`
   Dom string `json:"domain,omitempty" yaml:"domain,omitempty" bson:"domain,omitempty"`
   Role string `json:"role,omitempty" yaml:"role,omitempty" bson:"role,omitempty"`
}

func NewElement(t ElementType) *Element {
   return &Element{IntType: t}
}

// Types
const (
   Unset ElementType = iota
   Text
   Strong
   Emphasis
   Code
   Lit
   ULink
   VLink
   Role
   Sub
   Foot
)

func (e *Element) Type() string {
   switch e.IntType {
   case Text:
      return "inline-text"
   case Strong:
      return "strong"
   case Emphasis:
      return "emphasis"
   default:
      return fmt.Sprintf("Unset(%d)", e.IntType)
   }
}
