package ast

import (
   sitter "github.com/smacker/go-tree-sitter"
)


type Position struct {
   Start *Point `json:"position_start" yaml:"position_start" bson:"position_start"`
   End *Point `json:"position_stop" yaml:"position_stop" bson:"position_stop"`
}

func (e *Element) Position(node *sitter.Node){
   e.Pos = getPosition(node)
}

func getPosition(node *sitter.Node) *Position {
   start := node.StartPoint()
   end := node.EndPoint()
   return &Position{
      Start: &Point{
         Byte: node.StartByte(),
         Line: start.Row,
         Col: start.Column,
      },
      End: &Point{
         Byte: node.EndByte(),
         Line: end.Row,
         Col: end.Column,
      },
   }



}


type Point struct {
   Byte uint32 `json:"byte" yaml:"byte" bson:"byte"`
   Line uint32 `json:"line" yaml:"line" bson:"line"`
   Col uint32 `json:"column" yaml:"column" bson:"column"`
}

