package ast

type Section struct {
   Idrefs []string `json:"identity_references" yaml:"identity_references" bson:"identity_references"`
   Con *Content `json:"content" yaml:"content" bson:"content"`
   Pos *Position `json:"position" yaml:"position" bson:"position"`

   LiteralLevel string `json:"level_literal" yaml:"level_literal" bson:"literal_level"`
   LocalLevel int `json:"level_local" yaml:"level_local" bson:"level_local"`
   GlobalLevel int `json:"level_global" yaml:"level_global" bson:"level_global"`
}

func NewSection() *Section {
   return &Section{
      Idrefs: []string{},
      Con: NewContent(),
   }
}
